package com.ethercamp.harmony.dmt.db.ethereum;

import com.ethercamp.harmony.dmt.db.config.AppConfig;
import com.ethercamp.harmony.dmt.db.core.BlockCache;
import org.ethereum.core.BlockSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EthereumSend {

    private final Logger logger = LoggerFactory.getLogger("send");

    private static long counter = 0;
    private boolean syncDone = false;
    private BlockCache container;
    @Autowired
    private AppConfig appConfig;

    public void onBlock(BlockSummary blockSummary) {
        if(container==null){
            container = appConfig.blockCache();
        }
        this.container.add(blockSummary);
        if (++counter % 100 == 0){
            logger.info("onBlock() received {} blocks,  Latest number : {}",
                    counter, blockSummary.getBlock().getNumber());
        }
    }


    /**
     *  Mark the fact that you are touching
     *  the head of the chain
     */
    public void onSyncDone() {
        syncDone = true;
    }
}
