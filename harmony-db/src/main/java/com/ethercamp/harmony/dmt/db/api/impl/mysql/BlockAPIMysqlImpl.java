package com.ethercamp.harmony.dmt.db.api.impl.mysql;


import com.ethercamp.harmony.dmt.db.api.BlockAPI;
import com.ethercamp.harmony.dmt.db.core.SimpleBlock;

import java.util.List;

/**
 * @author leon.
 */

public class BlockAPIMysqlImpl implements BlockAPI {

    @Override
    public SimpleBlock getBlockByHash(String hash) {
        return null;
    }

    @Override
    public SimpleBlock getBlockByNumber(long blockNumber) {
        return null;
    }

    @Override
    public SimpleBlock getBlockByTransaction(String txHash) {
        return null;
    }

    @Override
    public List<SimpleBlock> getBlocksByRange(int start, int end) {
        return null;
    }

    @Override
    public List<SimpleBlock> getBlocksByTimestamp(long start, long end) {
        return null;
    }

    @Override
    public int getBlockMinedByMiner(String miner) {
        return 0;
    }
}
