package com.ethercamp.harmony.dmt.db.dao;


import com.ethercamp.harmony.dmt.db.core.SimpleBlock;

/**
 * Created by LiYang on 2016/11/22.
 */
public interface BlockTemplate{
    boolean exist(SimpleBlock block);
    SimpleBlock getBlock(Long number);
    int saveBlock(SimpleBlock block);
    void deleteBlock(SimpleBlock block);
    void updateBlock(SimpleBlock block);
    void dropBlockCollection();
}

