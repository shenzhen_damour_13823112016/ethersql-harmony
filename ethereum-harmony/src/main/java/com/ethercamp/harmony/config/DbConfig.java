/**
 * @System etherQL-harmony-1.9.0
 * @Description: Copyright: Copyright (c) 2018
 * Company: Aspire Technologies
 * @author zhongzifeng
 * @date 2018/12/17 15:07
 * @version V1.0
 */
package com.ethercamp.harmony.config;

import com.ethercamp.harmony.dmt.db.config.AppConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 增加
 * @author zhongzifeng
 * @date 2018/12/17 15:07
 *
 */
@Configuration
@ComponentScan(
        basePackages = "com.ethercamp.harmony")
public class DbConfig extends AppConfig {
}
